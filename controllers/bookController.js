const { Author, Book } = require("../models");

exports.getAllBook = async (req, res, next) => {
  try {
    const book = await Book.findAll({
      include: [
        {
          model: Author,
          as: "author",
        },
      ],
    });
    if (book.length !== 0) {
      res.status(200).json({
        status: true,
        message: "All Book retrieved",
        data: book,
      });
    } else {
      res.status(200).json({
        status: false,
        message: "Book still empty",
      });
    }
  } catch (error) {
    res.json(error);
  }
};

exports.getBook = async (req, res, next) => {
  try {
    const { id } = req.params;
    const book = await Book.findByPk(id, {
      include: [
        {
          model: Author,
          as: "author",
        },
      ],
    });
    if (book && book.id) {
      res.status(200).json({
        status: true,
        message: `Book with id ${id} retrieved`,
        data: book,
      });
    } else {
      res.status(200).json({
        status: false,
        message: `Book with id ${id} not found`,
      });
    }
  } catch (error) {
    res.json(error);
  }
};

exports.createBook = async (req, res, next) => {
  try {
    const authorId = req.body.authorId;
    const author = await Author.findByPk(authorId);
    if (author && author.id) {
      const book = await Book.create({
        title: req.body.title,
        description: req.body.description,
        authorId,
      });
      res.status(201).json({
        status: true,
        message: "Book created!",
        data: book,
      });
    } else {
      res.status(200).json({
        status: false,
        message: `Create book failed due to author with id ${authorId} not found`,
      });
    }
  } catch (error) {
    res.json(error);
  }
};

exports.updateBook = async (req, res, next) => {
  try {
    const { id } = req.params;
    if (req.body.authorId) {
      const authorId = req.body.authorId;
      const author = await Author.findByPk(authorId);
      if (author && author.id) {
        const book = await Book.findByPk(id);
        if (book && book.id) {
          const updatedBook = await book.update(
            {
              title: req.body.title || book.title,
              description: req.body.description || book.description,
              authorId: req.body.authorId || book.authorId,
            },
            {
              where: {
                id,
              },
            }
          );

          res.status(232).json({
            status: true,
            message: `Book with id ${id} updated!`,
            data: updatedBook,
          });
        } else {
          res.status(244).json({
            status: false,
            message: `Book with id ${id} not found!`,
          });
        }
      } else {
        res.status(244).json({
          status: false,
          message: `Update book with id ${id} failed due to authorId ${authorId} not found!`,
        });
      }
    } else {
      const book = await Book.findByPk(id);
      if (book && book.id) {
        const updatedBook = await book.update(
          {
            title: req.body.title || book.title,
            description: req.body.description || book.description,
          },
          {
            where: {
              id,
            },
          }
        );

        res.status(232).json({
          status: true,
          message: `Book with id ${id} updated!`,
          data: updatedBook,
        });
      } else {
        res.status(244).json({
          status: false,
          message: `Book with id ${id} not found!`,
        });
      }
    }
  } catch (error) {
    res.json(error);
  }
};

exports.deleteBook = async (req, res, next) => {
  try {
    const { id } = req.params;
    const book = await Book.destroy({
      where: {
        id,
      },
    });

    if (book) {
      res.status(233).json({
        status: true,
        message: `Book with id ${id} deleted!`,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Book with id ${id} not found!`,
      });
    }
  } catch (error) {
    res.json(error);
  }
};

// exports.getAllBookWithAuthorName = async (req, res, next) => {
//   try {
//     const { id } = req.params;
//     const book = await Book.findByPk(id);
//     if (book && book.id) {
//       console.log(`coba ${book.authorName}`);
//       res.status(200).json({
//         status: true,
//         message: `Book with id ${id} retrieved`,
//         data: book.authorName,
//       });
//     } else {
//       res.status(200).json({
//         status: false,
//         message: `Book with id ${id} not found`,
//       });
//     }
//   } catch (error) {
//     res.json(error);
//   }
// };
