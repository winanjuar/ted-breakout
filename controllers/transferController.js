const { sequelize, Amount, Transfer } = require("../models");

exports.doTransfer = async (req, res, next) => {
  let transaction;
  try {
    transaction = await sequelize.transaction();
    const dataTransfer = {
      amountSource: req.body.amountSource,
      amountTarget: req.body.amountTarget,
      nominal: Number(req.body.nominal),
    };

    const transfer = await Transfer.create(dataTransfer, { transaction });
    const amountSource = await Amount.findByPk(dataTransfer.amountSource);
    if (amountSource && amountSource.id) {
      const updateAmountSource = await amountSource.update(
        {
          balance: amountSource.balance - dataTransfer.nominal,
        },
        {
          where: {
            id: dataTransfer.amountSource,
          },
          transaction,
        }
      );
    } else {
      //jika amountSource tidak diketahui
    }

    const amountTarget = await Amount.findByPk(dataTransfer.amountTarget);
    if (amountTarget && amountTarget.id) {
      const updateAmountTarget = await amountTarget.update(
        {
          balance: amountTarget.balance + dataTransfer.nominal,
        },
        {
          where: {
            id: dataTransfer.amountTarget,
          },
          transaction,
        }
      );
    } else {
      //jika amountTarget tidak diketahui
    }

    await transaction.commit();

    res.status(201).json({
      status: true,
      message: "Transfer done!",
      data: transfer,
      amountSource: amountSource,
      amountTarget: amountTarget,
    });
  } catch (error) {
    if (transaction) await transaction.rollback();
    res.json(error);
  }
};
