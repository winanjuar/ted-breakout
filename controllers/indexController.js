exports.getLandingPage = (req, res, next) => {
  res.render("index", {
    title: "Telkomsel",
    event: "Software Developer Academy",
  });
};
