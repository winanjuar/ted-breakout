const { Author, Book } = require("../models");

exports.getAllAuthor = async (req, res, next) => {
  try {
    const author = await Author.findAll({
      include: [
        {
          model: Book,
          as: "books",
        },
      ],
    });
    if (author.length !== 0) {
      res.status(200).json({
        status: true,
        message: "All Author retrieved",
        data: author,
      });
    } else {
      res.status(200).json({
        status: false,
        message: "Author still empty",
      });
    }
  } catch (error) {
    res.json(error);
  }
};

exports.getAuthor = async (req, res, next) => {
  try {
    const { id } = req.params;
    const author = await Author.findByPk(id);
    if (author && author.id) {
      res.status(200).json({
        status: true,
        message: `Author with id ${id} retrieved`,
        data: author,
      });
    } else {
      res.status(200).json({
        status: false,
        message: `Author with id ${id} not found`,
      });
    }
  } catch (error) {
    res.json(error);
  }
};

exports.createAuthor = async (req, res, next) => {
  try {
    const author = await Author.create({
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
    });
    res.status(201).json({
      status: true,
      message: "Author created!",
      data: author,
    });
  } catch (error) {
    res.json(error);
  }
};

exports.createAuthorWithBook = async (req, res, next) => {
  try {
    const authorWithBook = {
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
      books: req.body.books,
    };

    const authorInserted = await Author.create(authorWithBook, {
      include: [
        {
          model: Book,
          as: "books",
        },
      ],
    });

    res.status(201).json({
      status: true,
      message: "Author with Books created!",
      data: authorInserted,
    });
  } catch (error) {
    res.json(error);
  }
};

exports.updateAuthor = async (req, res, next) => {
  try {
    const { id } = req.params;
    const author = await Author.findByPk(id);
    if (author && author.id) {
      const updatedAuthor = await author.update(
        {
          name: req.body.name || author.name,
          email: req.body.email || author.email,
          phone: req.body.phone || author.phone,
        },
        {
          where: {
            id,
          },
        }
      );

      res.status(232).json({
        status: true,
        message: `Author with id ${id} updated!`,
        data: updatedAuthor,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Author with id ${id} not found!`,
      });
    }
  } catch (error) {
    res.json(error);
  }
};

exports.deleteAuthor = async (req, res, next) => {
  try {
    const { id } = req.params;
    const author = await Author.destroy({
      where: {
        id,
      },
    });

    if (author) {
      res.status(233).json({
        status: true,
        message: `Author with id ${id} deleted!`,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Author with id ${id} not found!`,
      });
    }
  } catch (error) {
    res.json(error);
  }
};
