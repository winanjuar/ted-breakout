var express = require("express");
var router = express.Router();
const transferController = require("../controllers/transferController");

router.post("/do-transfer", transferController.doTransfer);

module.exports = router;
