var express = require("express");
var router = express.Router();
const authorController = require("../controllers/authorController");

router.get("/", authorController.getAllAuthor);
router.get("/:id", authorController.getAuthor);
router.post("/", authorController.createAuthor);
router.post("/with-books", authorController.createAuthorWithBook);
router.put("/:id", authorController.updateAuthor);
router.delete("/:id", authorController.deleteAuthor);

module.exports = router;
