"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Transfers", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      amountSource: {
        type: Sequelize.INTEGER,
        references: {
          model: "Amounts",
          key: "id",
        },
      },
      amountTarget: {
        type: Sequelize.INTEGER,
        references: {
          model: "Amounts",
          key: "id",
        },
      },
      nominal: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Transfers");
  },
};
