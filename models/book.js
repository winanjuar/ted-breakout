"use strict";
const { Model } = require("sequelize");
// const Author = require("./author");

module.exports = (sequelize, DataTypes) => {
  class Book extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Author, {
        foreignKey: "authorId",
        as: "author",
      });
    }
  }

  Book.init(
    {
      title: DataTypes.STRING,
      description: DataTypes.TEXT,
      authorId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Book",
    }
  );
  return Book;
};
